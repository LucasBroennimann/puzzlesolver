package configuration;

/**
 * This class holds all the parameters that can be changed ("Options" for the algorithm).
 * Those parameters directly impacts the results that will appear. Tweaking those parameters
 * can lead into better or worse results. 
 * 
 * The parameters here are described better in the documentation.
 */
public class Parameters {

	//BinaryImage: Border between "black & white". Everything within this range will be turned into white.
	public static final int[] BINARY_MIN_RANGE = { 236, 236, 236 };
	public static final int[] BINARY_MAX_RANGE = { 255, 255, 255 };
	
	//Number of pixel needed to count as PuzzlePiece
	public static final int MIN_PUZZLE_PIECE_PIXEL_AMOUNT = 300;
	
	//CornerDetection -> Square size around a corner. The bigger the dimensions, the bigger should the size be.
	public static final int FIND_CORNERS_SQUARE_SIZE = 40;	
	
	//Divisor to crop the puzzle piece rotation in height (y)
	public static final double HEIGHT_DIVISOR = 2.8;
	
	//Divisor to determine what an unwanted hole (scan error) that needs to be fixed, the bigger this number, the less holes will be filled
	public static final int HOLE_DIVISOR = 700;
	
	//Tolerance for the matching-algorithm. This could be calculated automatically in the future (related to the puzzle-piece size)
	public static final int TOLERANCE = 40;
	
	//Matching, divisor that determines if the sides match to each other by their dimension (to sort the pieces out before they match)
	//Depending on the dimension of a puzzle piece, this should be adjusted. The more a puzzle piece has a rectangular shape, the
	//more "strictness" is necessary here.
	//Matching-Divisor: The bigger this number, the smaller is the tolerance that determines, if one puzzle piece side should be compared
	//to another.
	//Standard value: 7
	public static final int DIVISOR_PUZZLE_SIDE = 7;
}
