package controller;

import java.io.File;
import java.util.List;
import model.Element;
import model.PuzzlePiece;
import controller.initialisation.BinaryImage;
import controller.initialisation.PreparePuzzle;
import controller.preparation.FindCorners;
import controller.preparation.FindGender;
import controller.preparation.PuzzlePieceOperations;
import controller.preparation.RotationMatrix;
import controller.processing.matching.Matching;

/**
 * Procedure:
 * ==========
 * 1. Create binary image
 * 2. Binary image -> 2D array (matrix)
 * 3. Search puzzlepieces -> Save
 * 4. Find each corners
 * 5. Save each puzzle piece 4 times (every rotation) and crop the pieces to the relevant part (more accureate Matching)
 * 6. Repair holes that came because of the matrix-rotation
 * 7. Find and save the gender of each rotation (male, female, border)
 * 8. Matching
 * 9. Visualisation
 * 
 * This class handles the whole process of the puzzle solver algorithm.
 * It's implemented to be used as Runnable.
 * 
 */
public class Controller implements Runnable {
	
	GUIController guiController;
	String imagePath;
	
	public Controller(GUIController guiController, String imagePath) {
		this.guiController = guiController;
		this.imagePath = imagePath;
	}
	
	@Override
	public void run() {
		assemble();
	}
	
	/**
	 * The main method activates the start() method. The main method will be used to start the GUI in the future.
	 */
	public void assemble() {
						
		Element firstCorner = startMatching();
		
		guiController.updateStatus("Draw result...");
		
		guiController.drawResult(firstCorner);
		
		guiController.updateStatus("Done!");
	}
	
	/**
	 * Steps through the whole matching-process.
	 */
	private Element startMatching() {
		
		long startTime = System.currentTimeMillis();
		System.out.println("0: " + (System.currentTimeMillis() - startTime));
		//1.
		guiController.updateStatus("Create binary image...");
		String binaryImagePath = BinaryImage.create(new File(imagePath).getParent(), new File(imagePath).getName());
		System.out.println("1: " + (System.currentTimeMillis() - startTime));
		
		//2.
		guiController.updateStatus("Convert binary image into Matrix...");
		int [][] binImgAsArray = BinaryImage.to2DArray(binaryImagePath);
		System.out.println("2: " + (System.currentTimeMillis() - startTime));
		
		//3.
		guiController.updateStatus("Search all puzzle pieces...");
		List<PuzzlePiece> pplist = new PreparePuzzle().searchPuzzlePieces(binImgAsArray);
		System.out.println("3: " + (System.currentTimeMillis() - startTime));
		
		//4.
		guiController.updateStatus("Find the corners...");
		new FindCorners().findTheCornersForEachPuzzlePiece(pplist);
		System.out.println("4: " + (System.currentTimeMillis() - startTime));
		
		//5.
		guiController.updateStatus("Rotate pieces...");
		new RotationMatrix().rotateAllPieces(pplist);
		guiController.updateStatus("Repair holes...");
		System.out.println("5: " + (System.currentTimeMillis() - startTime));
		
		//6.
		new PuzzlePieceOperations().repairHoles(pplist);
		System.out.println("6: " + (System.currentTimeMillis() - startTime));
		
		//7.
		guiController.updateStatus("Define the gender of each side...");
		new FindGender().findGender(pplist);
		System.out.println("7: " + (System.currentTimeMillis() - startTime));
		
		//8.
		guiController.updateStatus("Match all pieces......");
		Element firstCorner = new Matching().matchPieces(pplist);
		System.out.println("8: " + (System.currentTimeMillis() - startTime));
		
		//9...
		return firstCorner;
	}
}
