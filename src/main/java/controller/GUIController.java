package controller;

import javafx.scene.text.Text;
import model.Element;
import view.GUI;

public class GUIController {

	GUI gui;
	String imagePath;
	
	public GUIController(GUI gui, String imagePath) {
		this.gui = gui;
		this.imagePath = imagePath;
	}
	
	/**
	 * The assemble method is called by the "Assemble!"-Button in the GUI.class.
	 */
	public void assemble() {
		Controller controller = new Controller(this, imagePath);
//		Thread t = new Thread(controller);
//		t.start();
		controller.assemble();
	}

	/**
	 * This method draws the final result onto the GUI-ImageView.
	 * The idea is very basic and not fancy. It just provides information,
	 * but not in a fancy way.
	 * 
	 * @param firstCorner
	 */
	public void drawResult(Element firstCorner) {
		int x = 1;
		int y = 1;
			
		Element lastCorrect = null;
		for (Element cur = firstCorner; cur != null; cur = cur.down) {
			//Complete row...
			for (; cur != null; cur = cur.right) {
//				Pixel oc = cur.ppItem.getOriginalCoordinates();
				Text text = new Text("PuzzlePiece Nr.: " + cur.ppItem.puzzlePieceNumber + " = x: " + x++ + " / y: " + y);
				
				gui.addSolutionEntry(text);
				lastCorrect = cur;
			}
			cur = lastCorrect;
			++y;
			x = 1;
			
			//jump to first piece in row...
			while (cur.left != null) {
				cur = cur.left;
			}
		
		}
	}
	
	/**
	 * This methods should update the status text on the GUI. Since we didn't handle concurrency
	 * in this project, this method doesn't work at it's supposed to. It prints only the last
	 * status.
	 * 
	 * @param status
	 */
	public void updateStatus(String status) {
		gui.setStatus(status);
	}

}
