package controller;

import java.util.Arrays;

/**
 * Simple/generic operations that are used by different classes. 
 */
public class SimpleOperations {
	
	/**
	 * Creates a deep copy of an array.
	 * 
	 * @param m = Array to clone
	 * @return cloned array
	 */
	public static int[][] deepIntArrayCopy(int[][] m) {
		
//		int[][] temp = Arrays.stream(m).map((int[] row) -> row.clone()).toArray((int length) -> new int[length][]);
		
		int[][] temp = new int[m.length][m[0].length];
		
		for (int y=0; y<temp.length; ++y) {
			for (int x=0; x<temp[y].length; ++x) {
				temp[y][x] = m[y][x];
			}
		}
		
		return temp;
		
	}
}
