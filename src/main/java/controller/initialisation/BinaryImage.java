package controller.initialisation;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.bytedeco.javacpp.opencv_core.CvSize;
import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacpp.opencv_core.CvScalar;
import configuration.Parameters;
import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_highgui.*;

/**
 * Create a binary image (black/white) and save it under the same path as the old
 * picture. There will be a 2D-Array for every piece later.
 *
 */
public class BinaryImage {
	
	/**
	 * Takes an image, turns it into a binary image and stores it in the "working directory".
	 * 
	 * @param workingPath
	 * @param currentFileName
	 * @return
	 */
	public static String create(String workingPath, String currentFileName) {
		
		String newFileName = "binary_" + currentFileName;

		String origFilePath = workingPath + "\\" + currentFileName;
		String newFilePath = workingPath + "\\" + newFileName;
		
		//Range to determine if black/white.
		CvScalar min = cvScalar(Parameters.BINARY_MIN_RANGE[0], Parameters.BINARY_MIN_RANGE[1], Parameters.BINARY_MIN_RANGE[2], 0);
		CvScalar max= cvScalar(Parameters.BINARY_MAX_RANGE[0], Parameters.BINARY_MAX_RANGE[1], Parameters.BINARY_MAX_RANGE[2], 0);
		
        //Read image
        IplImage orgImg = cvLoadImage(origFilePath);
        //Create binary image of original size
        CvSize cvSize = cvGetSize(orgImg);
        
        IplImage newImg = cvCreateImage(cvSize, 8, 1);
        //Apply thresholding
        cvInRangeS(orgImg, min, max, newImg);
        //Save to path
        cvSaveImage(newFilePath, newImg);
           
        return newFilePath;
    }

	/**
	 * 
	 * Takes a binary image and creates a 2D array out of it. The values are 0 for "white" and 1 for "black".
	 * This means, that all the 1 are either part of a puzzle piece or a scan error (scan errors will be ignored.
	 * 
	 * @param binaryImagePath
	 * @return
	 */
	public static int[][] to2DArray(String binaryImagePath) {
		BufferedImage img = null;
		try {
			img = ImageIO.read(new File(binaryImagePath));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
		int[][] arr = new int[img.getHeight()][img.getWidth()];
		
		//Iterate from left to right, top to bottom (like a book) through the matrix
		for(int i = 0; i < arr.length; i++)
		    for(int j = 0; j < arr[i].length; j++)
		    	//If black, set the value to 1
		    	if (img.getRGB(j, i) == -16777216)
		    		arr[i][j] = 1;
				//If white, set the value to 0
		    	else if (img.getRGB(j, i) == -1)
		    		arr[i][j] = 0;
		return arr;
	}
}