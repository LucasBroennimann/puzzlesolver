package controller.initialisation;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import configuration.Parameters;
import model.Pixel;
import model.PuzzlePiece;

/**
 * This class is designed to prepare the puzzle pieces. The puzzle pieces get extracted and
 * put into single "PuzzlePiece" objects.
 */
public class PreparePuzzle {
	
	public int minY = 0;
	public int maxY = 0;
	public int minX = 0;
	public int maxX = 0;
	
	/**
	 * Runs through the whole binary image. If the value 1 is found, the algorithm checks with a search algorithm, if
	 * it's a puzzle piece or just a scan error. Puzzle pieces are converted into "PuzzlePiece" objects. 
	 * 
	 * @param arr, pplist
	 */
	public List<PuzzlePiece> searchPuzzlePieces(int[][] arr) {
		final List<PuzzlePiece> pplist = new ArrayList<>();
		
		//Number of the found PuzzlePiece
		int puzzlePieceNumber = 0;
		for (int y=0; y<arr.length; ++y) {
			for (int x=0; x<arr[y].length; ++x) {
				if (arr[y][x] == 1) {
					
					int pixelCounter = searchNeighboringPixels(x, y, arr);
					
					if (pixelCounter > Parameters.MIN_PUZZLE_PIECE_PIXEL_AMOUNT) {
						PuzzlePiece pp = new PuzzlePiece();
						pp.setBoundingBoxCoordinatesFromOriginalPicture(minX, maxX, minY, maxY);
						pp.setPuzzleMatrix(maxX-minX+1, maxY-minY+1);
						pp.setOriginPosition(x, y);
						pp.setNumber(++puzzlePieceNumber);
						
						//Cuts the Puzzle piece from the binary image (arr) and pastes it into the puzzle object
						arr = getThePuzzlePiece(x, y, arr, pp);
						
						pplist.add(pp);
					} else {
						deleteFakePuzzlePiece(x, y, arr);
					}
				}
			}
		}
		return pplist;
	}
	
	/**
	 * Looks if a pixel leads to a complete puzzle piece.
	 * - If the algorithm detects, that it found a puzzle piece, it converts it into a PuzzlePiece object and removes it from
	 * the 2D-Array.
	 * - If the algorithm detects, that he found a scan error (found pixels < tolerance to be a puzzle piece), it removes it
	 * from the 2D-Array.
	 * 
	 * @param x
	 * @param y
	 * @param arr
	 * @return 
	 */
	private int searchNeighboringPixels(int x, int y, int[][] arr) {
		//Set the bounding box to the start of the found pixel
		minX = x;
		maxX = x;
		minY = y;
		maxY = y;
		int pixelCounter = 0;
		
		Stack<Pixel> pixels = new Stack<>();
		pixels.push(new Pixel(x,y));
		while (!pixels.isEmpty()) {
			Pixel p = pixels.pop();
			x = p.getX();
			y = p.getY();
			arr[y][x] = 2;
			
			if (x+1 < arr[y].length && arr[y][x+1] == 1) {
				pixels.push(new Pixel(x+1,y));
				++pixelCounter;
				if (x+1 > maxX) maxX = x+1;
			}
			if (x-1 >= 0 && arr[y][x-1] == 1) {
				pixels.push(new Pixel(x-1,y));
				++pixelCounter;
				if (x-1 < minX) minX = x-1;
			}
			if (y+1 < arr.length && arr[y+1][x] == 1) {
				pixels.push(new Pixel(x,y+1));
				++pixelCounter;
				if (y+1 > maxY) maxY = y+1;
			}
			if (y-1 >= 0 && arr[y-1][x] == 1) {
				pixels.push(new Pixel(x,y-1));
				++pixelCounter;
				if (y-1 < minY) minY = y-1;
			}
		}
		
		return pixelCounter;
	}
	
	
	/**
	 * This method is called when the "puzzle piece search-algorithm" found a puzzle piece. It
	 * directly sets the matrix in the puzzle piece object that has been generated before.
	 * 
	 * @param x
	 * @param y
	 * @param arr
	 * @param pp
	 */
	private int[][] getThePuzzlePiece(int x, int y, int[][] arr, PuzzlePiece pp) {
		Stack<Pixel> stack = new Stack<>();
		stack.push(new Pixel(x,y));
		while (!stack.isEmpty()) {
			Pixel p = stack.pop();
			x = p.getX();
			y = p.getY();
			pp.imageMatrix[y-minY][x-minX] = 1;
			arr[y][x] = 0;
			if (x+1 < arr[y].length && arr[y][x+1] == 2) {
				stack.push(new Pixel(x+1, y));
			}
			if (x-1 >= 0 && arr[y][x-1] == 2) {
				stack.push(new Pixel(x-1, y));
			}
			if (y+1 < arr.length && arr[y+1][x] == 2) {
				stack.push(new Pixel(x, y+1));
			}
			if (y-1 >= 0 && arr[y-1][x] == 2) {
				stack.push(new Pixel(x, y-1));
			}
		}
		return arr;
	}
	
	
	/**
	 * This method is called if the puzzle piece search-algorithm found a scan error. It removes
	 * the errors from the binary-matrix.
	 * 
	 * @param x
	 * @param y
	 * @param arr
	 */
	private void deleteFakePuzzlePiece(int x, int y, int[][] arr) {
		Stack<Pixel> stack = new Stack<>();
		stack.push(new Pixel(x,y));
		while (!stack.isEmpty()) {
			Pixel p = stack.pop();
			x = p.getX();
			y = p.getY();
			arr[y][x] = 0;
			if (x+1 < arr[y].length && arr[y][x+1] == 1) {
				stack.push(new Pixel(x+1,y));
			}
			if (x-1 >= 0 && arr[y][x-1] == 1) {
				stack.push(new Pixel(x-1,y));
			}
			if (y+1 < arr.length && arr[y+1][x] == 1) {
				stack.push(new Pixel(x,y+1));
			}
			if (y-1 >= 0 && arr[y-1][x] == 1) {
				stack.push(new Pixel(x,y-1));
			}
		}
	}
}
