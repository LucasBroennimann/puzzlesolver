package controller.preparation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import configuration.Parameters;
import model.*;


/**
 * Approach:
 * 1. Turn the binary image into a 2D Array (of "Pixels").
 * 2. Iterate through the 2D Array and cut out the single puzzle pieces.
 * 
 * 3. Iterate through all PuzzlePieces
 *      For every pixel that is part of the puzzle piece, search a square around it (specific
 *      pixel in the center) and count the pixels that are part of the puzzle piece. Connect
 *      the number of pixels with the specifiy pixel (saved as "Point").
 *      Put all of this points into a list and sort it ascending.
 *      The Point in this list a position 0 is the point with the lowest amount of "black" pixels.
 *      The first point is also the first corner in the puzzlepiece. Index 1 in this list could 
 *      now be a neigbor of the first pixel. Our additional requirement from now on is, that other
 *      corners have a minimum distance to already found corners. The position of the Corners are
 *      saved in the "PuzzlePiece" class.
 *      
 * Future "benefits":
 * - After the corners are found, they will be used to "turn" the piece into a straight direction.
 * - From there on, they can be compared to other pieces. Binding the edges together.
 */
public class FindCorners {

	/**
	 * This method runs through all puzzle pieces and finds its corners.
	 * 
	 * Concept: Around every puzzle piece pixel, there will be calculated a score based on a square around the specific
	 * pixel. The pixels with the least amount of "1" are corners (see documentation). To prevent that pixels next
	 * to each other get chosen as different corners, a "new" corner always needs to have a minimum distance to all
	 * other found points.
	 * 
	 * @implementation All corners are saved in a counter clockwise order.
	 * @param pplist
	 */
	public void findTheCornersForEachPuzzlePiece(List<PuzzlePiece> pplist) {
		pplist.parallelStream().forEach(this::findCorners);
	}

	private void findCorners(PuzzlePiece pp) {
		//Put all Pixels (Point) that are part of a puzzle piece (== value 1) into a list (every pixel then has a "value" which represents
		//the corner-score (number of "1" around it). The lower this score is, the less neighboring pixels a pixel has. 
		List<Pixel> pointList = new ArrayList<>(pp.imageMatrix.length / 2);
		for (int y=0; y<pp.imageMatrix.length; ++y) {
			for (int x=0; x<pp.imageMatrix[y].length; ++x) {
				if (pp.imageMatrix[y][x] == 1) {
					Pixel point = new Pixel(x,y);
				
					//Check how many real neighbouring pixels (value == 1) are around a corner and save it into the pointList.
					int value = checkIfThisPixelIsCorner(pp, x, y);
					point.setNrOfNeighbouringPixels(value);
					pointList.add(point);
				}
			}
		}
		
		//Every corner needs a minimum distance to the other corners (to prevent 2 corners being neighbors).
		int minDistance = pp.imageMatrix.length / 2;
		
		//Sort. The best value (least amount of 1 around it) is the first corner.
		Collections.sort(pointList);	//Interface Comparable in "PuzzlePiece" class.
		
		//Take the first corner and delete it from the poinLlist.
		Pixel pt = Collections.min(pointList);
		pointList.remove(pt);
		
		//Search the other corners...
		int index = 0;
		pp.corners[index] = pt;
		for (Pixel point : pointList) {
			if (validDistance(pp.corners, point, minDistance)) {
				
				if (index+1 > 3) break;
				
				pp.corners[++index] = point;
			}
		}
		
		//Sort the corners. The order of the corners (clock-/counter-clockwise) isn't decided here... Only sort to length-1, since the last
		//entry is already sorted by then.
		for (int i=0; i<pp.corners.length-1; ++i) {
			int x = pp.corners[i].getX();
			int y = pp.corners[i].getY();
			
			int shortestDistance = Integer.MAX_VALUE;
			for (int j=i+1; j<pp.corners.length; ++j) {
				//distance == hypotenuse
				int distance = (int) Math.round(Math.sqrt(Math.pow(x - pp.corners[j].getX(), 2) + Math.pow(y - pp.corners[j].getY(), 2)));
				if (distance < shortestDistance) { 
					Pixel temp = pp.corners[i+1];
					pp.corners[i+1] = pp.corners[j];
					pp.corners[j] = temp;
					shortestDistance = distance;
				}
			}
		}
		
		//Sort the corners so that they lay counter-clockwise in the corner array.
		int minX = pp.corners[0].getX();
		int minY = pp.corners[0].getY();
		int maxX = pp.imageMatrix[0].length - pp.corners[0].getX() - 1;
		int maxY = pp.imageMatrix.length - pp.corners[0].getY() - 1;
		//Check, to which edge the points lays most near.
		Integer[] distancesToBorders = { minX, minY, maxX, maxY };
		List<Integer> l = Arrays.asList(distancesToBorders);
		int min = Collections.min(l);
		
		//Smallest distance to the western wall, +Y neighbour = superior
		if (minX == min) {
			if (pp.corners[1].getY() < pp.corners[3].getY()) {
				Pixel temp = pp.corners[1];
				pp.corners[1] = pp.corners[3];
				pp.corners[3] = temp;
			}
		} 
		//Smallest distance to the northern wall, -X neighbour = superior 
		else if (minY == min) {
			if (pp.corners[1].getX() > pp.corners[3].getX()) {
				Pixel temp = pp.corners[1];
				pp.corners[1] = pp.corners[3];
				pp.corners[3] = temp;
			}
		}
		//Smallest distance to the eastern wall, -Y neighbour = superior
		else if (maxX == min) {
			if (pp.corners[1].getY() > pp.corners[3].getY()) {
				Pixel temp = pp.corners[1];
				pp.corners[1] = pp.corners[3];
				pp.corners[3] = temp;
			}
		}
		//Smallest distance to the southern wall, +X neighbour = superior
		else if (maxY == min) {
			if (pp.corners[1].getX() < pp.corners[3].getX()) {
				Pixel temp = pp.corners[1];
				pp.corners[1] = pp.corners[3];
				pp.corners[3] = temp;
			}
		}
		
		//This can logically never happen and is just here as a test...
		else {
			throw new IllegalStateException("Houston, we have a problem... FindCorners failed!");
		}
	}
	
	/**
	 * Checks the distance a Pixel has to another one.
	 * 
	 * @param origin = Corners that are already found
	 * @param newPoint
	 * @param minDistance
	 * @return true if distance is bigger than the minDistance.
	 */
	private static boolean validDistance(Pixel[] origin, Pixel newPoint, int minDistance) {
		
		//== c^2
		minDistance *= minDistance;
		
		for (int i=0; i<origin.length; ++i) {
			
			if (origin[i] == null) continue;
			
			//Positive kathete x
			int x = (origin[i].getX() - newPoint.getX() > 0) ? origin[i].getX() - newPoint.getX() : newPoint.getX() - origin[i].getX();
			//Positive kathete y
			int y = (origin[i].getY() - newPoint.getY() > 0) ? origin[i].getY() - newPoint.getY() : newPoint.getY() - origin[i].getY();
			//Pythagoras: a^2 + b^2 = c^2
			int distance = x * x + y * y;
			
			//If the minDistance  is higher than the calculated distance, return false ("not  a real corner, but
			//a neighbor of an existing corner").
			if (distance < minDistance) return false;
		}
		
		return true;
	}
	
	/**
	 * Check a square with the Pixel(x,y) as center and count the score (numbers of found "1"-values). 
	 * 
	 * @param pp
	 * @param x
	 * @param y
	 * @return number of pixels (value: 1) that were found
	 */	
	private static int checkIfThisPixelIsCorner(PuzzlePiece pp, int x, int y) {
		int numberOneCounter = 0;
		
		for (int i = y-Parameters.FIND_CORNERS_SQUARE_SIZE / 2; i <= y + Parameters.FIND_CORNERS_SQUARE_SIZE / 2; ++i) {
			if (i < 0) continue;
			if (i >= pp.imageMatrix.length) break;
			for (int j = x - Parameters.FIND_CORNERS_SQUARE_SIZE / 2; j <= x + Parameters.FIND_CORNERS_SQUARE_SIZE / 2; ++j) {
				if (j < 0) continue;
				if (j >= pp.imageMatrix[i].length) break;
				if (pp.imageMatrix[i][j] == 1) {
					++numberOneCounter;
				}
			}				
		}
		return numberOneCounter;
	}
}
