package controller.preparation;

import java.util.List;
import model.Gender;
import model.PuzzlePiece;

public class FindGender {
	
	/**
	 * Iterates through the pplist and sets the gender for each piece.
	 * 
	 * @param pplist
	 */
	public void findGender(List<PuzzlePiece> pplist){
		pplist.parallelStream().forEach(this::findGender);
	}

	private void findGender(PuzzlePiece pp) {
		for (int index = 0; index < pp.rotated.length; index++) {
			int[][] arr = pp.rotated[index].matrix;
			pp.rotated[index].gender = checkGender(arr);
		}
	}
		
	/**
	 * Defines the gender of a side from a puzzle piece.2
	 * 
	 * @param arr
	 * @return
	 */
	private Gender checkGender(int[][] arr) {
	
		int zeroCounter = 0;
		int oneCounter = 0;
		
		int inc = arr[0].length / 10;
		for (int i=inc; i<arr[0].length - inc; i+=inc) {
			if (arr[arr.length/3][i] == 0) ++zeroCounter;
			else if (arr[arr.length/3][i] == 1) ++oneCounter;
		}
		
		Gender g;
		
		if (zeroCounter == 0) g = Gender.BORDER;
		else if (zeroCounter >= oneCounter) g = Gender.MALE;
		else g = Gender.FEMALE;
		
		return g;
	}
}
	