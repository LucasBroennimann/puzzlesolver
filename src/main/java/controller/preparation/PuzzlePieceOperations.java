package controller.preparation;

import java.util.List;
import java.util.Stack;
import configuration.Parameters;
import model.Pixel;
import model.PuzzlePiece;


public class PuzzlePieceOperations {

	/**
	 * Repair small holes (some of them created by the matrix rotation).
	 * 
	 * @param pplist
	 * @return
	 */
	public void repairHoles(List<PuzzlePiece> pplist) {
		pplist.parallelStream().forEach(this::repairHoles);
	}

	private void repairHoles(PuzzlePiece pp) {
		for (int i=0; i<pp.rotated.length; ++i) {
			int[][] current = pp.rotated[i].matrix;
			
			//Fill "natural" holes... (Scan errors & rotation errors)
			for (int y=0; y<current.length; ++y) {
				for (int x=0; x<current[y].length; ++x) {
					if (current[y][x] == 0) {
						//Search
						int pxCounter = searchNeighboringPixels(x, y, current, 0, 2);
						
						//If small hole, repair! If big hole, it's probably a wanted hole like the lock-part of a puzzle
						if (pxCounter < current.length * current[y].length / Parameters.HOLE_DIVISOR) {
							searchNeighboringPixels(x, y, current, 2, 1);
						}
					}
				}
			}
			
			//Set all int == 2 values back to 0
			for (int y=0; y<current.length; ++y) {
				for (int x=0; x<current[y].length; ++x) {
					if (current[y][x] == 2) current[y][x] = 0; 
				}
			}
			
			//Fill the sides of a piece if the shape is concave. Also fill some body parts from other sides.
			for (int idx=0; idx<current[0].length; ++idx) {
				if (current[current.length-1][idx] == 0)
					searchNeighboringPixels(idx, current.length-1, current, 0, 2);
			}
			
			//Correct the unnecessary sides.
			for (int y=0; y<current.length; ++y) {
				for (int x=0; x<current[y].length; ++x) {
					if (current[y][x] == 2) current[y][x] = 1; 
				}
			}
		}
	}
	
	/**
	 * A fill-algorithm that searches neighboring pixels of a kind.
	 * 
	 * @param x
	 * @param y
	 * @param arr
	 * @param infectedNumber
	 * @param filler
	 * @return
	 */
	private int searchNeighboringPixels(int x, int y, int[][] arr, int infectedNumber, int filler) {
		int pixelCounter = 0;
		
		Stack<Pixel> pixels = new Stack<>();
		pixels.push(new Pixel(x,y));
		while (!pixels.isEmpty()) {
			Pixel p = pixels.pop();
			x = p.getX();
			y = p.getY();
			arr[y][x] = filler;
			
			if (x+1 < arr[y].length && arr[y][x+1] == infectedNumber) {
				pixels.push(new Pixel(x+1,y));
				++pixelCounter;
			}
			if (x-1 >= 0 && arr[y][x-1] == infectedNumber) {
				pixels.push(new Pixel(x-1,y));
				++pixelCounter;
			}
			if (y+1 < arr.length && arr[y+1][x] == infectedNumber) {
				pixels.push(new Pixel(x,y+1));
				++pixelCounter;
			}
			if (y-1 >= 0 && arr[y-1][x] == infectedNumber) {
				pixels.push(new Pixel(x,y-1));
				++pixelCounter;
			}
		}
		
		return pixelCounter;
	}

	/**
	 * Crops the rotation of the pieces to the specific area (for the matching-alrogithm).
	 * 
	 * @param pplist
	 * @return
	 */
	public void cropRotatedPieces(List<PuzzlePiece> pplist) {
		
		for (PuzzlePiece pp : pplist) {
			for (int i=0; i<pp.rotated.length; ++i) {
				
				int[][] temp = pp.rotated[i].matrix;
				
				int corner1X = pp.rotated[i].corner1.getX();
				int corner2X = pp.rotated[i].corner2.getX();
				
				//3 = 1/3 of the puzzle piece = Y, corner2X - corner1X = X
				int[][] cropped = new int[(int) ((double) temp.length / Parameters.HEIGHT_DIVISOR)][corner2X - corner1X + 1];
				
				for (int y=0; y<cropped.length; ++y) {
					for (int x=corner1X; x<=corner2X; ++x) {
						cropped[y][x-corner1X] = temp[y][x];
					}
				}
				
				//After cropping, set the position of the new corners.
				pp.rotated[i].corner1 = new Pixel(pp.rotated[i].corner1.getX() - corner1X, pp.rotated[i].corner1.getY());
				pp.rotated[i].corner2 = new Pixel(pp.rotated[i].corner2.getX() - corner1X, pp.rotated[i].corner2.getY());

				//Overwrite the rotated puzzle piece with the cropped version.
				pp.rotated[i].matrix = cropped;
			}
		}
	}
}
