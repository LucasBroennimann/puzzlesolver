package controller.preparation;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import configuration.Parameters;
import controller.SimpleOperations;
import model.Matrix;
import model.Pixel;
import model.PuzzlePiece;

/**
 * This class rotates all found pieces into 4 rotations (north, east, south, west) all with
 * direction up.
 */
public class RotationMatrix {

	/**
	 * This is the controller that handles each step.
	 * 
	 * @param pplist
	 * @return
	 */
	public void rotateAllPieces(List<PuzzlePiece> pplist) {
		pplist.parallelStream().forEach(this::rotatePiece);	
	}

	private void rotatePiece(PuzzlePiece pp) {
		for (int currentCornerIndex = 0; currentCornerIndex < pp.corners.length; ++currentCornerIndex) {
			Pixel anglePoint = pp.corners[currentCornerIndex];
			Pixel otherPoint = pp.corners[(currentCornerIndex+1) % pp.corners.length];
			
			//Distance between "distance" and "anglePoint".
			int distancePointToAnglePoint = (int) Math.round(Math.sqrt(Math.pow(anglePoint.getX() - otherPoint.getX(), 2) + Math.pow(anglePoint.getY() - otherPoint.getY(), 2)));
			double degree = getDegree(anglePoint, otherPoint);
			
			double[] rotationMatrix = getRotationMatrix(degree);
			
			rotateMatrix(pp.imageMatrix, anglePoint, rotationMatrix, pp, currentCornerIndex, distancePointToAnglePoint);
			
			pp.rotated[currentCornerIndex].matrix = createContentAlignedBoundingBox(pp, currentCornerIndex);
		}
	}
	
	private double getDegree(Pixel anglePoint, Pixel otherPoint) {
		
		int virtX = otherPoint.getX() - anglePoint.getX();
		int virtY = otherPoint.getY() - anglePoint.getY();
				
		double degree = Math.toDegrees(Math.atan2(virtY, virtX));
		
		return degree;
	}

	private void rotateMatrix(int[][] originMatrix, Pixel anglePoint, double[] rotationMatrix, PuzzlePiece pp, int currentCornerIndex, int distanceToAnglePoint) {
		
		//The puzzlepiece has a rectangular shape. The "tempMatrix" needs to be big enough to handle a rotation.
		//To get a good minimal distance, take the contour-distance that has the biggest distance to the "anglePoint".
		int[] distances = new int[4];
		distances[0] = anglePoint.getX();							//Distance to western wall
		distances[1] = originMatrix[0].length - anglePoint.getX();	//Distance to eastern wall
		distances[2] = anglePoint.getY();							//Distance to nothern wall
		distances[3] = originMatrix.length - anglePoint.getY();		//Distance to southern wall

		Arrays.sort(distances);
		
		//To get a single center point, we need a length (array) that is odd.
		int length = (3 * distances[distances.length - 1]) % 2 == 0 ? 3 * distances[distances.length - 1] + 1 : 3 * distances[distances.length - 1];
		
		//tempMatrix has a square-shape for the rotation
		int[][] tempMatrix = new int[length][length];
		
		int tempNullX = tempMatrix[0].length / 2;
		int tempNullY = tempMatrix.length / 2;
		
		for (int y=0; y<originMatrix.length; ++y) {
			for (int x=0; x<originMatrix[y].length; ++x) {
				if (originMatrix[y][x] == 1) {
					
					int virtX = x - anglePoint.getX();
					int virtY = anglePoint.getY() - y;
					
					Pixel newVirtPos = rotateSinglePixel(rotationMatrix, new Pixel(virtX, virtY));
					tempMatrix[tempNullY + newVirtPos.getY()/**-1*/][tempNullX + newVirtPos.getX()] = 1;
				}
			}
		}
		
		//Rotate the corners too...
		pp.rotated[currentCornerIndex] = new Matrix(pp, tempMatrix);
		pp.rotated[currentCornerIndex].corner1 = new Pixel(tempNullX, tempNullY); //AnglePoint is exactly in the middle (tempMatrix)
		pp.rotated[currentCornerIndex].corner2 = new Pixel(pp.rotated[currentCornerIndex].corner1.getX() + distanceToAnglePoint, pp.rotated[currentCornerIndex].corner1.getY());
		
		Pixel newAnglePoint = new Pixel(tempNullX, tempNullY);
		
		pp.setNewAnglePoint(newAnglePoint);	
	}

	/**
	 * Returns a content aligned bounding boxed 2D Matrix.
	 * 
	 * @param newAnglePoint
	 * @param tempMatrix
	 * @return
	 */
	private int[][] createContentAlignedBoundingBox(PuzzlePiece pp, int rotatedIndex) {
				
		//This is a definitve point with the value 1. From here, you can evaluate the whole puzzle piece.
		Pixel newAnglePoint = pp.getNewAnglePoint();
		
		//Deep copy of rotation
		int[][] tempMatrix = SimpleOperations.deepIntArrayCopy(pp.rotated[rotatedIndex].matrix);

		//Set the bounding box to the start of the found pixel
		int minX = newAnglePoint.getX();
		int maxX = newAnglePoint.getX();
		int minY = newAnglePoint.getY();
		int maxY = newAnglePoint.getY();
		
		Stack<Pixel> pixels = new Stack<>();
		pixels.push(newAnglePoint);
		while (!pixels.isEmpty()) {
			Pixel p = pixels.pop();
			int x = p.getX();
			int y = p.getY();
			
			//The if statement is needed! There can be 2x the same point in the Stack<Point> because we check
			//in 8 directions for every point.
			if (tempMatrix[y][x] != 2) {
				tempMatrix[y][x] = 2;
				if (x+1 < tempMatrix[y].length && tempMatrix[y][x+1] == 1) {
					pixels.push(new Pixel(x+1,y));
					if (x+1 > maxX) maxX = x+1;
				}
				if (x-1 >= 0 && tempMatrix[y][x-1] == 1) {
					pixels.push(new Pixel(x-1,y));
					if (x-1 < minX) minX = x-1;
				}
				if (y+1 < tempMatrix.length && tempMatrix[y+1][x] == 1) {
					pixels.push(new Pixel(x,y+1));
					if (y+1 > maxY) maxY = y+1;
				}
				if (y-1 >= 0 && tempMatrix[y-1][x] == 1) {
					pixels.push(new Pixel(x,y-1));
					if (y-1 < minY) minY = y-1;
				}
			}
		}
		
		int corner1X = pp.rotated[rotatedIndex].corner1.getX();
		int corner2X = pp.rotated[rotatedIndex].corner2.getX();
		
		
		int height = (int) ((double) (maxY - minY + 1) / Parameters.HEIGHT_DIVISOR);
		int[][] cropped = new int[height][corner2X - corner1X + 1];
				
		for (int y=minY; y < minY+height; ++y)
			for (int x=corner1X; x <= corner2X; ++x)
				cropped[y-minY][x-corner1X] = pp.rotated[rotatedIndex].matrix[y][x];
		
		pp.rotated[rotatedIndex].corner1 = new Pixel(0, pp.rotated[rotatedIndex].corner1.getY() - minY);
		pp.rotated[rotatedIndex].corner2 = new Pixel(cropped[0].length-1, pp.rotated[rotatedIndex].corner2.getY() - minY);
		
		return cropped;
	}

	
	/**
	 * Calculates the rotation-matrix that is needed to rotate another matrix.
	 * 
	 * Help: https://en.wikipedia.org/wiki/Rotation_matrix
	 * 
	 * @param degree
	 * @return
	 */
	private double[] getRotationMatrix(double degree) {
		double[] rm = new double[4];
		
		rm[0] = Math.cos(Math.toRadians(degree));
		rm[1] = Math.sin(Math.toRadians(degree)) * -1;
		rm[2] = Math.sin(Math.toRadians(degree));
		rm[3] = Math.cos(Math.toRadians(degree));
				
		return rm;
	}
	
	/**
	 * Rotates a single "Pixel" using a rotation matrix. The rotation can be counter-clockwise AND clockwise,
	 * depending on if the degree is positive or negative
	 * 
	 * @param rotationMatrix
	 * @param virtPoint
	 */
	private Pixel rotateSinglePixel(double[] rotationMatrix, Pixel virtPoint) {
		double x = Math.round(rotationMatrix[0] * virtPoint.getX() + rotationMatrix[1] * virtPoint.getY());
		double y = Math.round(rotationMatrix[2] * virtPoint.getX() + rotationMatrix[3] * virtPoint.getY());
		
		return new Pixel((int) x, (int) y);
	}
}
