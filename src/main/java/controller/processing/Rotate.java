package controller.processing;

import model.Matrix;
import model.Pixel;

public class Rotate {
	
	private static Matrix rotate90(Matrix m) {
		
		final int yRef = m.matrix.length;
	    final int xRef = m.matrix[0].length;
	    int[][] ret = new int[xRef][yRef];
	    for (int y = 0; y < yRef; y++) {
	        for (int x = 0; x < xRef; x++) {
	            ret[x][yRef-1-y] = m.matrix[y][x];
	        }
	    }
	    m.matrix = ret;
	    return m;
	}
	
	public static void rotate180(Matrix m) {

		//Rotate90 twice
		m = rotate90(m);
		m = rotate90(m);
		
		//Reverse each row
		for (int y=0; y<m.matrix.length; ++y) {
			for (int x=0; x<m.matrix[y].length; ++x) {
				int t = m.matrix[y][x];
				m.matrix[y][x] = m.matrix[y][m.matrix[y].length - x - 1];
				m.matrix[y][m.matrix[y].length - x - 1] = t;
			}
		}
		
		//Reverse each column
		for (int y=0; y<m.matrix.length; ++y) {
			for (int x=0; x<m.matrix[y].length; ++x) {
				int t = m.matrix[y][x];
				m.matrix[y][x] = m.matrix[m.matrix.length - y - 1][x];
				m.matrix[m.matrix.length - y - 1][x] = t;
			}
		}
		
		m = rotateCorners180(m);
	}

	/**
	 * This method is ready for the corner-change (save corners in the matrix class).
	 * 
	 * @param m
	 */
	private static Matrix rotateCorners180(Matrix m) {
		int x = m.matrix[0].length;
		int y = m.matrix.length;
		
		//Flip horizontal
		m.corner1 = new Pixel(x - m.corner1.getX() - 1, m.corner1.getY());
		m.corner2 = new Pixel(x - m.corner2.getX() - 1, m.corner2.getY());
		
		//Flip vertical
		m.corner1 = new Pixel(m.corner1.getX(), y - m.corner1.getY() - 1);
		m.corner2 = new Pixel(m.corner2.getX(), y - m.corner2.getY() - 1);
		
		return m;
	}

}
