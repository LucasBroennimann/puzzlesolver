package controller.processing.matching;

import java.util.ArrayList;
import java.util.List;
import configuration.Parameters;
import controller.processing.Rotate;
import model.Element;
import model.Gender;
import model.Matrix;
import model.PuzzlePiece;
import model.Score;

/**
 * Controller for the border piece matching.
 * This is the first of two parts of the matching algorithm. Matches the whole border.
 * 
 * 1. Rotate the chosen puzzle piece side 180 degree around.
 * 2. Copy it in a new array where both pieces fit in.
 * 3. Add the other piece and count the mismatches.
 *
 */
public class BorderMatching {
	
	//*** For test purposes.
	public static int perfectMatchCounter = 0;
	public static int goodMatchCounter = 0;
	public static int okMatchCounter = 0;
	public static int badMatchCounter = 0;
	public static int poorMatchCounter = 0;
	//***
	
	private int puzzleLengthCounter = 1;
	private int puzzleHeightCounter = 1;

	/**
	 * Borderpiece-matching
	 * 
	 * @param pplist
	 * @return
	 */
	public Element matchBorderPieces(List<PuzzlePiece> pplist) {
		

		//Pieces with a flat border. => ArrayList. CornerPieces are marked as such.
		//Set the index of the border-side in the pp.rotated[] array.
		List<PuzzlePiece> bplist = popPiecesWithBorder(pplist);

		//Match all pieces and create a score list.
		bplist = calculatePossibleMatches(bplist);
		
		//Puzzle piece-sides that clearly point to each other.
		bplist = markDefinitiveMatches(bplist);
		
		//Decide for the rest of the matches.
		bplist = calculateAllMatches(bplist);
		
		//Turn the list into a linkedlist for a better capsulation.
		Element firstCorner = parseIntoLinkedList(bplist); 
		
		for (PuzzlePiece bp : bplist) {
			System.out.println(bp.puzzlePieceNumber + " = " + bp.northIndex);
		}
		
		//Check if the algorithm was successful
		if (checkIfSuccess(bplist)) {
			return firstCorner;
		}		
		else {
			return null;
		}
	}
		
	
	/**
	 * Turns the puzzle pieces into a LinkedList. The list has pointers in 4 directions (up, right, down, left).
	 * 
	 * @param bplist
	 * @return
	 */
	public Element parseIntoLinkedList(List<PuzzlePiece> bplist) {

		//Instantiate element-linkedlist, the first CornerPiece found in the bplist is chosen for the upper left corner.
		//It doesn't matter if this doesn't make sense to the human eye (-> puzzle can be upside down in the end result).
		Element firstCorner = null;
		for (PuzzlePiece pp : bplist) {
			if (pp.isCornerPiece) {
				firstCorner = new Element(pp);
				break;
			}
		}
		
		int mod = firstCorner.ppItem.rotated.length;

		PuzzlePiece fc = firstCorner.ppItem;
		fc.setNorth(fc.borderIndex);
				
		Element currentElement = firstCorner;
		
		//Upper left to upper right
		currentElement.right = new Element(currentElement.ppItem.rotated[Math.floorMod(currentElement.ppItem.northIndex-1, mod)].foundMatch.parentPP);
		currentElement.right.left = currentElement;
		System.out.println("Der Reihe nach: " + currentElement.ppItem.rotated[Math.floorMod(currentElement.ppItem.northIndex-1, mod)].foundMatch.parentPP.puzzlePieceNumber);
		currentElement = currentElement.right;
		++puzzleLengthCounter;
		while (!currentElement.ppItem.isCornerPiece) {
			++puzzleLengthCounter;
			currentElement.ppItem.setNorth(currentElement.ppItem.borderIndex);
			currentElement.right = new Element(currentElement.ppItem.rotated[Math.floorMod(currentElement.ppItem.northIndex-1, mod)].foundMatch.parentPP);
			System.out.println("Der Reihe nach: " + currentElement.ppItem.rotated[Math.floorMod(currentElement.ppItem.northIndex-1, mod)].foundMatch.parentPP.puzzlePieceNumber);
			currentElement.right.left = currentElement;
			currentElement = currentElement.right;
		}
		
		
		//Upper right to lower right
		currentElement.ppItem.setNorth(Math.floorMod(currentElement.ppItem.borderIndex+1, mod));
		currentElement.down = new Element(currentElement.ppItem.rotated[Math.floorMod(currentElement.ppItem.northIndex+2, mod)].foundMatch.parentPP);
		System.out.println("Der Reihe nach: " + currentElement.ppItem.rotated[Math.floorMod(currentElement.ppItem.northIndex+2, mod)].foundMatch.parentPP.puzzlePieceNumber);
		currentElement.down.up = currentElement;
		currentElement = currentElement.down;
		++puzzleHeightCounter;
		while (!currentElement.ppItem.isCornerPiece) {
			++puzzleHeightCounter;
			currentElement.ppItem.setNorth(Math.floorMod(currentElement.ppItem.borderIndex+1, mod));
			currentElement.down = new Element(currentElement.ppItem.rotated[Math.floorMod(currentElement.ppItem.northIndex+2, mod)].foundMatch.parentPP);
			System.out.println("Der Reihe nach: " + currentElement.ppItem.rotated[Math.floorMod(currentElement.ppItem.northIndex+2, mod)].foundMatch.parentPP.puzzlePieceNumber);
			currentElement.down.up = currentElement;
			currentElement = currentElement.down;
		}
		
		
		//Lower right to lower left
		int puzzleLengthCounter2 = puzzleLengthCounter;
		currentElement.ppItem.setNorth(Math.floorMod(currentElement.ppItem.borderIndex+2, mod));
		currentElement.left = new Element(currentElement.ppItem.rotated[Math.floorMod(currentElement.ppItem.northIndex+1, mod)].foundMatch.parentPP);
		currentElement.left.right = currentElement;
		System.out.println("Der Reihe nach: " + currentElement.ppItem.rotated[Math.floorMod(currentElement.ppItem.northIndex+1, mod)].foundMatch.parentPP.puzzlePieceNumber);
		currentElement = currentElement.left;
		--puzzleLengthCounter2;
		while (!currentElement.ppItem.isCornerPiece) {
			--puzzleLengthCounter2;
			currentElement.ppItem.setNorth(Math.floorMod(currentElement.ppItem.borderIndex+2, mod));
			currentElement.left = new Element(currentElement.ppItem.rotated[Math.floorMod(currentElement.ppItem.northIndex+1, mod)].foundMatch.parentPP);
			System.out.println("Der Reihe nach: " + currentElement.ppItem.rotated[Math.floorMod(currentElement.ppItem.northIndex+1, mod)].foundMatch.parentPP.puzzlePieceNumber);
			currentElement.left.right = currentElement;
			currentElement = currentElement.left;
		}
		
		//Lower left to upper left
		int puzzleHeightCounter2 = puzzleHeightCounter;
		currentElement.ppItem.setNorth(Math.floorMod(currentElement.ppItem.borderIndex-1, mod));
		currentElement.up = new Element(currentElement.ppItem.rotated[Math.floorMod(currentElement.ppItem.northIndex, mod)].foundMatch.parentPP);
		currentElement.up.down = currentElement;
		System.out.println("Der Reihe nach: " + currentElement.ppItem.rotated[Math.floorMod(currentElement.ppItem.northIndex, mod)].foundMatch.parentPP.puzzlePieceNumber);
		currentElement = currentElement.up;
		--puzzleHeightCounter2;
		//If there is another corner piece (it has to be!), the border piece matching is complete.
		while (!currentElement.ppItem.isCornerPiece) {
			--puzzleHeightCounter2;
			currentElement.ppItem.setNorth(Math.floorMod(currentElement.ppItem.borderIndex-1, mod));
			currentElement.up = new Element(currentElement.ppItem.rotated[currentElement.ppItem.northIndex].foundMatch.parentPP); //1. corner exists now 2 times... fixed down below!
			System.out.println("Der Reihe nach: " + currentElement.ppItem.rotated[Math.floorMod(currentElement.ppItem.northIndex, mod)].foundMatch.parentPP.puzzlePieceNumber);
			currentElement.up.down = currentElement;
			currentElement = currentElement.up;
		}
		
		//If true, all sides are equally long.
		assert (puzzleLengthCounter2 == 1 && puzzleHeightCounter2 == 1);
		
		//Fix the issue, that the first corner exists 2x as an Element
		firstCorner.down = currentElement.down;
		currentElement.down.up = firstCorner;
		
		return firstCorner;
	}
	

	/**
	 * Check if the sides on the left/right of a border have a match.
	 * If one specific side doesn't have a "foundmatch", the algorithm failed somewhere. 
	 */
	private boolean checkIfSuccess(List<PuzzlePiece> bplist) {
		
		for (PuzzlePiece bp : bplist) {
			//Counter-Clockwise
			Matrix ccm = bp.isCornerPiece ? bp.rotated[Math.floorMod(bp.borderIndex+2, bp.rotated.length)] : bp.rotated[Math.floorMod(bp.borderIndex+1, bp.rotated.length)];
			//Clockwise
			Matrix cm = bp.rotated[Math.floorMod(bp.borderIndex-1, bp.rotated.length)];
			
			if (ccm.foundMatch == null || cm.foundMatch == null) return false;
		}
		return true;
	}


	/**
	 * Search for the best matches for the pieces, that do not point to each other.
	 * 
	 * @param bplist
	 * @return
	 */
	private List<PuzzlePiece> calculateAllMatches(List<PuzzlePiece> bplist) {
		
		//Second run, for pieces that haven't been found yet. -> Documentation as an interface for other algorithms.
		for (PuzzlePiece bp : bplist) {
			
			Matrix bpm = bp.rotated[Math.floorMod(bp.borderIndex-1, bp.rotated.length)];
			
			if (bpm.foundMatch == null) {
				
				//Good Match level... Documentation
				if ((	 bpm.bestScores[1].matrix.foundMatch == null
						&& bpm == bpm.bestScores[1].matrix.bestScores[0].matrix)
						//If the following line gives a "NullPointerException", the matching above failed.
						|| (bpm.bestScores[0].matrix.foundMatch == null
						&& bpm == bpm.bestScores[0].matrix.bestScores[1].matrix))
				{
					int score1 = Integer.MAX_VALUE/2;
					int score2 = Integer.MAX_VALUE/2;
					
					if (	bpm.bestScores[1].matrix.foundMatch == null
							&& bpm == bpm.bestScores[1].matrix.bestScores[0].matrix)
					{
						score1 = bpm.bestScores[1].score;
					}
					if (	bpm.bestScores[0].matrix.foundMatch == null
							&& bpm == bpm.bestScores[0].matrix.bestScores[1].matrix)
					{
						score2 = bpm.bestScores[0].score;
					}
					
					if (score1 <= score2) {
						bpm.foundMatch = bpm.bestScores[1].matrix;
						bpm.bestScores[1].matrix.foundMatch = bpm;
					} else {
						bpm.foundMatch = bpm.bestScores[0].matrix;
						bpm.bestScores[0].matrix.foundMatch = bpm;
					}
				}
			}
		}
				
		for (PuzzlePiece bp : bplist) {			
			
			Matrix bpm = bp.rotated[Math.floorMod(bp.borderIndex-1, bp.rotated.length)];
			
			if (bpm.foundMatch == null) {

				// OK Match level... Documentation
				if ((	bpm.bestScores[1].matrix.foundMatch == null
						&& bpm == bpm.bestScores[1].matrix.bestScores[1].matrix)
						
						|| (bpm.bestScores[0].matrix.foundMatch == null
						&& bpm == bpm.bestScores[0].matrix.bestScores[2].matrix)
						
						|| bpm.bestScores[2].matrix.foundMatch == null
						&& bpm == bpm.bestScores[2].matrix.bestScores[0].matrix)
				{

					++okMatchCounter;

					int score1 = Integer.MAX_VALUE / 2;
					int score2 = Integer.MAX_VALUE / 2;
					int score3 = Integer.MAX_VALUE / 2;

					if (	bpm.bestScores[1].matrix.foundMatch == null
							&& bpm == bpm.bestScores[1].matrix.bestScores[1].matrix)
					{
						score1 = bpm.bestScores[1].score;
					}
					if (	bpm.bestScores[0].matrix.foundMatch == null
							&& bpm == bpm.bestScores[0].matrix.bestScores[2].matrix)
					{
						score2 = bpm.bestScores[0].score;
					}
					if (	bpm.bestScores[2].matrix.foundMatch == null
							&& bpm == bpm.bestScores[2].matrix.bestScores[0].matrix)
					{
						score3 = bpm.bestScores[2].score;
					}

					if (score1 <= score2 && score1 <= score3) {
						bpm.bestScores[1].matrix.foundMatch = bpm;
						bpm.foundMatch = bpm.bestScores[1].matrix;
					} else if (score2 <= score1 && score2 <= score3) {
						bpm.bestScores[0].matrix.foundMatch = bpm;
						bpm.foundMatch = bpm.bestScores[0].matrix;
					} else {
						bpm.foundMatch = bpm.bestScores[2].matrix;
						bpm.bestScores[2].matrix.foundMatch = bpm;
					}
				}
			}
		}

		for (PuzzlePiece bp : bplist) {
			
			Matrix bpm = bp.rotated[Math.floorMod(bp.borderIndex-1, bp.rotated.length)];
			
			if (bpm.foundMatch == null) {

				// Bad match level...
				if ((	bpm.bestScores[1].matrix.foundMatch == null
						&& bpm == bpm.bestScores[1].matrix.bestScores[2].matrix)
						
						|| (bpm.bestScores[2].matrix.foundMatch == null
						&& bpm == bpm.bestScores[2].matrix.bestScores[1].matrix))
				{

					++badMatchCounter;

					int score1 = Integer.MAX_VALUE / 2;
					int score2 = Integer.MAX_VALUE / 2;

					if (	bpm == bpm.bestScores[1].matrix.bestScores[2].matrix
							&& bpm.bestScores[1].matrix.foundMatch == null)
					{
						score1 = bpm.bestScores[1].score;
					}

					if (bpm.bestScores[2] != null
							&& bpm.bestScores[2].matrix.foundMatch == null
							&& bpm == bpm.bestScores[2].matrix.bestScores[1].matrix)
					{
						score2 = bpm.bestScores[2].score;
					}

					if (score1 <= score2) {
							bpm.foundMatch = bpm.bestScores[1].matrix;
							bpm.bestScores[1].matrix.foundMatch = bpm;
					} else {
							bpm.foundMatch = bpm.bestScores[2].matrix;
							bpm.bestScores[2].matrix.foundMatch = bpm.foundMatch;
					}
				}
			}
		}

		for (PuzzlePiece bp : bplist) {
			
			Matrix bpm = bp.rotated[Math.floorMod(bp.borderIndex-1, bp.rotated.length)];
			
			if (bpm.foundMatch == null) {
				// Poor match level...
				if (bpm.bestScores[2].matrix.foundMatch == null
					&& bpm == bpm.bestScores[2].matrix.bestScores[2].matrix)
				{

					++poorMatchCounter;

					bpm.foundMatch = bpm.bestScores[2].matrix;
					bpm.bestScores[2].matrix.foundMatch = bpm;
				}
			}
		}

		return bplist;
	}


	/**
	 * Goes through the the matched sides and check if there are any perfect matches. Mark them, so that they will be ignored in future tries.
	 */
	private List<PuzzlePiece> markDefinitiveMatches(List<PuzzlePiece> bplist) {
		for (PuzzlePiece bp : bplist) {
			
			Matrix bpm = bp.rotated[Math.floorMod(bp.borderIndex-1, bp.rotated.length)];
			
			//Mark as "found" if the side of the piece points to each other. With this, the pieces will be ignored in the next run.
			//If there is a side in the scoreboard[0] that matches to this side, declare both as "found". 
			if (bpm.bestScores[0].matrix.bestScores[0].matrix == bpm) {				
				++perfectMatchCounter;
				bpm.bestScores[0].matrix.foundMatch = bpm;
				bpm.foundMatch = bpm.bestScores[0].matrix;
			}
		}
		return bplist;
	}

	/**
	 * Matches all border pieces against each other.
	 * 
	 * @param bplist
	 * @return
	 */
	private List<PuzzlePiece> calculatePossibleMatches(List<PuzzlePiece> bplist) {
		
		/**
		 * Matching Clockwise: After that, all clockwise sides have a score-array with the 3 best matches (Sides from other puzzle pieces).
		 */
		for (PuzzlePiece bp : bplist) {
			//If piece == puzzlepiece, you need to jump 2 sides to not match a border to something else
			Matrix bpm = bp.rotated[Math.floorMod(bp.borderIndex-1, bp.rotated.length)];
			
			Rotate.rotate180(bpm);
			
			//The smaller the score, the better the result
			int score = Integer.MAX_VALUE/2;
							
			Score[] scoreboard = bpm.bestScores;
			
			for (PuzzlePiece comparePP : bplist) {
				Matrix cpm = comparePP.isCornerPiece ? comparePP.rotated[Math.floorMod(comparePP.borderIndex+2, comparePP.rotated.length)] : comparePP.rotated[Math.floorMod(comparePP.borderIndex+1, comparePP.rotated.length)];
				
				if (
					//Same Gender cannot match to each other...
					bpm.gender == cpm.gender
					//If the bp and comparePP are the same piece, they shouldn't be matched against each other 
					|| comparePP.equals(bp)
					//Only match sides that have around the same length.
					|| Math.abs(bpm.matrix[0].length - cpm.matrix[0].length) > (cpm.matrix[0].length / Parameters.DIVISOR_PUZZLE_SIDE)
					) continue;
				
				int currentScore = Matching.compare2Edges(bpm, cpm, score);
				
				if (scoreboard[2] == null || currentScore < scoreboard[2].score) {
					rearrangeScores(scoreboard, new Score(cpm, currentScore));
					
					if (scoreboard[0].score == currentScore) score = currentScore;
				}
			}
			
			//Rotate the main-piece back...
			Rotate.rotate180(bpm);
		}
		
		
		/**
		 * Matching Counter-Clockwise
		 */
		for (PuzzlePiece bp : bplist) {
			//If piece == puzzlepiece, you need to jump 2 sides to not match a border to something else
			Matrix bpm = bp.isCornerPiece ? bp.rotated[Math.floorMod(bp.borderIndex+2, bp.rotated.length)] : bp.rotated[Math.floorMod(bp.borderIndex+1, bp.rotated.length)];
			
			Rotate.rotate180(bpm);
			
			//The smaller the score, the better the result
			int score = Integer.MAX_VALUE/2;
							
			Score[] scoreboard = bpm.bestScores;
			
			for (PuzzlePiece comparePP : bplist) {
				Matrix cpm = comparePP.rotated[Math.floorMod(comparePP.borderIndex-1, comparePP.rotated.length)];
				
				if (
						//Same Gender cannot match to each other...
						bpm.gender == cpm.gender
						//If the bp and comparePP are the same piece, they shouldn't be matched against each other 
						|| comparePP.equals(bp)
						//Only match rectangle sides that have around the same length.
						|| Math.abs(bpm.matrix[0].length - cpm.matrix[0].length) > (cpm.matrix[0].length / Parameters.DIVISOR_PUZZLE_SIDE)
						) continue;
				
				int currentScore = Matching.compare2Edges(bpm, cpm, score);
				
//				if (scoreboard[0] == null || currentScore < scoreboard[0].score + Parameters.TOLERANCE) {
				if (scoreboard[2] == null || currentScore < scoreboard[2].score) {
					rearrangeScores(scoreboard, new Score(cpm, currentScore));
					
					if (scoreboard[0].score == currentScore) score = currentScore;
				}
			}
			
			//Rotate the mainpiece back...
			Rotate.rotate180(bpm);
		}
		
		return bplist;
	}


	/**
	 * Manages the array with the 3 nearest Scores to each other.
	 * Could be changed to a while loop for a variable scoreboard size.
	 * 
	 * @param scoreboard
	 * @param currentScore
	 */
	private void rearrangeScores(Score[] scoreboard, Score currentScore) {
		if (scoreboard[0] == null || currentScore.score < scoreboard[0].score) {
			scoreboard[2] = scoreboard[1];
			scoreboard[1] = scoreboard[0];
			scoreboard[0] = currentScore;
		} else if (scoreboard[1] == null || currentScore.score < scoreboard[1].score) {
			scoreboard[2] = scoreboard[1];
			scoreboard[1] = currentScore;
		} else if (scoreboard[2] == null || currentScore.score < scoreboard[2].score) {
			scoreboard[2] = currentScore;
		}
	}

	/**
	 * - Pops borderpieces out of the pplist.
	 * - Marks cornerpieces
	 * - Sets the index of the border for each piece (rotation[])
	 * 
	 * @param pplist
	 * @return
	 */
	private List<PuzzlePiece> popPiecesWithBorder(List<PuzzlePiece> pplist) {
		List<PuzzlePiece> borderPiecesList = new ArrayList<>();
		List<PuzzlePiece> markForRemove = new ArrayList<>();
		
		for (PuzzlePiece pp : pplist) {
			int borderCounter = 0;
			int borderIndex = -1;
			for (int i=0; i<pp.rotated.length; ++i) {
				if (pp.rotated[i].gender == Gender.BORDER) {
					++borderCounter;
					pp.borderIndex = i;
				}
			}
			
			if (borderCounter > 0) {
				pp.isBorderPiece = true;
				borderPiecesList.add(pp);
				markForRemove.add(pp);
			}
			//Define as corner piece if it has 2 borders.
			if (borderCounter == 2) {
				pp.isCornerPiece = true;
				//If 2 borders, choose the border on the counter-clockwise side
				if (pp.rotated[Math.floorMod(borderIndex - 1, pp.rotated.length)].gender == Gender.BORDER) {
					pp.borderIndex = Math.floorMod(pp.borderIndex - 1, pp.rotated.length);
				}
			}
		}
		
		pplist.removeAll(markForRemove);
				
		assert (borderPiecesList.size() % 2 == 0);
		return borderPiecesList;
	}

	public int getPuzzleLength() {
		return puzzleLengthCounter;
	}
	
	public int getPuzzleHeight() {
		return puzzleHeightCounter;
	}

}
