package controller.processing.matching;

import java.util.List;

import configuration.Parameters;
import controller.processing.Rotate;
import model.Element;
import model.Matrix;
import model.PuzzlePiece;

/**
 * Controller for the inner Matching.
 * This is the second of two steps. After the border-match is complete, match all the inner pieces (puzzle pieces
 * without a border-side).
 */
public class InnerMatching {
	
	/**
	 * Matches all the inner pieces, after the border-matching is done. So the "frame" must exist.
	 * 
	 * @param firstCorner
	 * @param pplist
	 * @param puzzleLength
	 * @param puzzleHeight
	 * @return
	 */
	public boolean matchInnerPieces(Element firstCorner, List<PuzzlePiece> pplist, int puzzleLength, int puzzleHeight) {
		
		Element cur = firstCorner.down;
		
		int mod = cur.ppItem.rotated.length;	//== 4
		
		for (int i=0; i<puzzleHeight-2; ++i) {
			for (int j=0; j<puzzleLength-2; ++j) {
				
				Matrix right = cur.ppItem.rotated[Math.floorMod(cur.ppItem.northIndex-1, mod)];
				Matrix down = cur.up.right.ppItem.rotated[Math.floorMod(cur.up.right.ppItem.northIndex+2, mod)];
				
				//Find the matching piece and set the pointers to it (right + down)
				Element maybeEndOfRow = cur.up.right.right.down == null ? null : cur.up.right.right.down;
				cur.right = findNeighboringMatch(right, down, pplist, maybeEndOfRow);
				cur.up.right.down = cur.right;
				
				//Set the pointers from the newly found piece
				cur.right.left = cur;
				cur.right.up = cur.up.right;
				
				//End of row?
				if (cur.up.right.right.ppItem.isBorderPiece && cur.up.right.right.down != null) {
					cur.right.right = cur.up.right.right.down;
					cur.up.right.right.down.left = cur.right;
				}
				
				//move on...
				cur = cur.right;
			}
			
			//The end of a row needs to be a border piece.
//			if (!cur.right.ppItem.isBorderPiece) throw new IllegalStateException("The matching process failed!");
			if (!cur.right.ppItem.isBorderPiece) return false;
			
			while (!cur.left.ppItem.isBorderPiece) {
				cur = cur.left;
			}
			cur = cur.left.down;
			
			//Last middle piece row needs to be connected to the southern border.
			if (cur.ppItem.isCornerPiece) {
				cur = cur.right;
				while (!cur.ppItem.isCornerPiece) {
					cur.up = cur.left.up.right;
					cur.left.up.right.down = cur;
					
					cur = cur.right;
				}
			}
		}
		
		return true;
	}

	/**
	 * Searches a match for 2 sides. If the piece is the last before a borderpiece, 3 sides are checked.
	 * 
	 * @param right
	 * @param down
	 * @param pplist
	 * @param maybeEndOfRow
	 * @return
	 */
	private Element findNeighboringMatch(Matrix right, Matrix down, List<PuzzlePiece> pplist, Element maybeEndOfRow) {
		int bestMatchingScore = Integer.MAX_VALUE / 2;
		PuzzlePiece bestMatch = null;

		for (PuzzlePiece pp : pplist) {
			for (int i=0; i<pp.rotated.length; ++i) {
				
				if (	//Does a site already have a final match?
						pp.rotated[i].foundMatch != null || pp.rotated[Math.floorMod(i+1, pp.rotated.length)].foundMatch != null
						//Do the genders match?
						|| pp.rotated[i].gender == down.gender || pp.rotated[Math.floorMod(i+1, pp.rotated.length)].gender == right.gender
						//Can the sides (length) match naturally?
						|| Math.abs(pp.rotated[i].matrix[0].length - down.matrix[0].length) > (down.matrix[0].length / Parameters.DIVISOR_PUZZLE_SIDE)
						
						//If the matched piece is the last one before the border, there are 3 sides to match... check if the gender is right.
						//This part is documented in the "Mittelteile-Matching" section of the documentation.
						|| (maybeEndOfRow != null
						&& maybeEndOfRow.ppItem.rotated[maybeEndOfRow.ppItem.northIndex+1].gender
						== pp.rotated[Math.floorMod(i-1, pp.rotated.length)].gender)
						) continue;
				
				Rotate.rotate180(down);
				Rotate.rotate180(right);
				
				int score1 = Matching.compare2Edges(down, pp.rotated[i], bestMatchingScore);
				int score2 = Matching.compare2Edges(right, pp.rotated[(i+1) % pp.rotated.length], bestMatchingScore);
				int score3 = 0;	//Currently unused, explained in the documentation in the description of the matching algorithm.
				
				Rotate.rotate180(down);
				Rotate.rotate180(right);
				
				if ((score1 + score2 + score3) < bestMatchingScore) {
					pp.setNorth(i);
					bestMatch = pp;
					bestMatchingScore = score1 + score2 + score3;
				}
			}
		}
		
		if (bestMatch == null) throw new IllegalStateException("Middle-Piece match failed... (bestMatch == null)");
		
		bestMatch.rotated[bestMatch.northIndex].foundMatch = down;
		down.foundMatch = bestMatch.rotated[bestMatch.northIndex];
		
		bestMatch.rotated[(bestMatch.northIndex+1) % bestMatch.rotated.length].foundMatch = right;
		right.foundMatch = bestMatch.rotated[(bestMatch.northIndex+1) % bestMatch.rotated.length];
		
		pplist.remove(bestMatch);
		
		return new Element(bestMatch);
	}
}
