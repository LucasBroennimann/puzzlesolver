package controller.processing.matching;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import model.Element;
import model.Matrix;
import model.Pixel;
import model.PuzzlePiece;
import configuration.Parameters;
import controller.SimpleOperations;

/**
 * Controller-class for the whole matching process. It iterates through all the necessary steps.
 */
public class Matching {
	
	/**
	 * Starter method for the matching process. 
	 * 
	 * @param pplist
	 * @return
	 */
	public Element matchPieces(List<PuzzlePiece> pplist) {	
		//This deletes the borderpieces out of the pplist
		BorderMatching bm = new BorderMatching();
		Element firstCorner = bm.matchBorderPieces(pplist);
		
		if (firstCorner == null) throw new RuntimeException("BorderPieces, Matching failure...");
				
		int puzzleLength = bm.getPuzzleLength();
		int puzzleHeight = bm.getPuzzleHeight();
		InnerMatching im = new InnerMatching();
		boolean innerMatchSuccess = im.matchInnerPieces(firstCorner, pplist, puzzleLength, puzzleHeight);
		
		if (! innerMatchSuccess)
			throw new RuntimeException("MiddlePieces, Matching failure...");
		else 
			
			
		return firstCorner;
	}
	
	
	/**
	 * Takes 2 "Matrix" objects and compares them to each other.
	 * 
	 * @param main = Matrix 1
	 * @param other = Matrix 2
	 * @param maxScore = The algorithm aborts if the max score gets topped.
	 * @return matching score
	 */
	public static int compare2Edges(Matrix main, Matrix other, int maxScore) {
		
		int score = 0;
		
		//See the documentation for this...
		int lengthY = main.corner1.getY() + 1 + other.matrix.length - other.corner1.getY();
		int lengthX = (main.matrix[0].length < other.matrix[0].length) ? main.matrix[0].length : other.matrix[0].length;
		
		int[][] matchArray = new int[lengthY][lengthX];
		
		//Copy the "main" array into the matching array
		for (int y=0; y<matchArray.length; ++y) {
			if (y >= main.matrix.length) break;
			for (int x=0; x<matchArray[y].length; ++x) {
				matchArray[y][x] = main.matrix[y][x];
			}
		}
		
		
		//1. Run through the by the "other"-side unreached part of the matching array and count all 0 as mismatch.
		for (int y=0; y < lengthY - other.matrix.length; ++y) {
			for (int x=0; x < lengthX; ++x) {
				if (main.matrix[y][x] == 0) ++score;
			}
		}
		
		//This points is needed to find the same index in 2 different sized arrays
		Pixel relativePoint = new Pixel(0, matchArray.length - other.matrix.length);
		
		//2. The floodQueue runs through "other"-array and compares the pixel with the "main" piece.
		//Queue is needed to get a fill-algorithm. This leads to a faster detection if a piece will be a mismatch.
		Queue<Pixel> floodQueue = new LinkedList<>();

		Pixel startPoint = new Pixel(lengthX / 2, matchArray.length - other.matrix.length);
		floodQueue.add(startPoint);
		
		
		//Create a deep copy of other.rotated[otherIndex], to manipulate without manipulating the original array.
		//We need to mark visited points here with a 2, but this does only affect this encapsulated operation.
		int[][] otherArrayCopy = SimpleOperations.deepIntArrayCopy(other.matrix);
		//Compare the other- with the matching array.
		while (!floodQueue.isEmpty()) {
			Pixel p = floodQueue.remove();
			int x = p.getX();
			int y = p.getY();

			//If at one pixel the value is the same == mismatch, there are only the values 0 and 1.
			if (other.matrix[y-relativePoint.getY()][x] == matchArray[y][x]) {
				++score;
			}
			//Stop the process, if the mismatch counter is to high anyways to get a match.
			if (score > maxScore + Parameters.TOLERANCE) {
				return Integer.MAX_VALUE/2;
			}
			
			//Flood to the other pixels...
			//East...
			if (x + 1 < matchArray[y].length /*&& x + 1 < otherArrayCopy[0].length */&& otherArrayCopy[y-relativePoint.getY()][x+1] != 2) {
				floodQueue.add(new Pixel(x+1, y));
				otherArrayCopy[y-relativePoint.getY()][x+1] = 2;
			}
			//South...
			if (y + 1 < matchArray.length && otherArrayCopy[y-relativePoint.getY()+1][x] != 2) {
				floodQueue.add(new Pixel(x, y+1));
				otherArrayCopy[y-relativePoint.getY()+1][x] = 2;
			}
			//West...
			if (x - 1 >= 0 && otherArrayCopy[y-relativePoint.getY()][x-1] != 2) {
				floodQueue.add(new Pixel(x-1, y));
				otherArrayCopy[y-relativePoint.getY()][x-1] = 2;
			}
		}
		
		return score;
	}
}
