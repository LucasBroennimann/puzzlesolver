package model;

/**
 * The elements that form a linked list.
 */
public class Element {
	
	public final PuzzlePiece ppItem;
	
	public Element left, right, up, down = null;
	
	public Element(PuzzlePiece pp) {
		this.ppItem = pp;
	}

}
