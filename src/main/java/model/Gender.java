package model;

/**
 * This enum is used to define the gender or a puzzle piece side.
 */
public enum Gender {

	BORDER,
	MALE,
	FEMALE;
	
}
