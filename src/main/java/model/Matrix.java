package model;

/**
 * The Matrix class represents a side of a puzzle piece.
 * Each puzzle pieces therefore contains 4 sides => 4 Matrix instances.
 */
public class Matrix {

	//The PuzzlePiece that holds this side.
	public final PuzzlePiece parentPP;
	
	//2D array representation of an unrotated piece.
	public int [][] matrix;

	//If this side has a found match (!= null) then it will be ignored by the matching process
	public Matrix foundMatch = null;
	
	//The corners of the puzzle piece visible within this isde
	public Pixel corner1 = null;
	public Pixel corner2 = null;
	
	//The gender of this side
	public Gender gender = null;
	
	//For the border matching. A ranking containing the most probably matching sides from other pieces.
	public Score[] bestScores = new Score[3];
	
	public Matrix (PuzzlePiece pp, int[][] matrix) {
		this.parentPP = pp;
		this.matrix = matrix;
	}
	
	public Gender getGender() {
		return this.gender;
	}
}
