package model;

/**
 * Point is an Object that represents a single Pixel. It also contains a "value", that represents
 * the amount of neighbouring black pixels.
 */
public class Pixel implements Comparable<Pixel> {
	private int x;
	private int y;
	
	//Number of neighboring pixels, used to determine if a point is a corner
	private int nrOfNeighbouringPixels;
	
	public Pixel(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void setNrOfNeighbouringPixels(int value) {
		this.nrOfNeighbouringPixels = value;
	}
	
	public int getX() { return this.x; }
	public int getY() { return this.y; }
	public int getValue() { return this.nrOfNeighbouringPixels; }

	/**
	 * compareTo method, so that the points in the list can be sorted according to their value.
	 */
	@Override
	public int compareTo(Pixel p) {
		if (this.nrOfNeighbouringPixels == p.nrOfNeighbouringPixels) return 0;
		return this.nrOfNeighbouringPixels > p.getValue() ? 1 : -1;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null) return false;
		Pixel p = (Pixel) o;
		
		return p.getX() == this.x && p.getY() == this.y;
	}
	
	public Pixel clone() {
		return new Pixel(x,y);
	}
}
