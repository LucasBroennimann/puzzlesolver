package model;


/**
 * Soll das spätere PuzzleTeil im einzelnen repräsentieren. Diese Klasse ist sehr
 * provisorisch implementiert.
 *
 */
public class PuzzlePiece {
	
	public int puzzlePieceNumber;
	
	//Where is the piece to be found in the original picture?
	private Pixel originPosition = null;

	public boolean isCornerPiece = false;
	public boolean isBorderPiece = false;
	public int borderIndex = -1;	//Default initialisation, -1 would lead to an IndexOutOfBounds-Exception
	public int northIndex = -1;		//Default initialisation, -1 would lead to an IndexOutOfBounds-Exception

	//Corners of the unrotated puzzle piece
	public Pixel[] corners = new Pixel[4];
	
	//This point is needed for creating a minimal bounding box of a rotation
	Pixel newAnglePoint;
	
	//Bounnding Box
	int minX, maxX, minY, maxY;
	
	//Original puzzle piece untrotated
	public int[][] imageMatrix;
	
	//The 4 sides of a puzzle piece
	public Matrix[] rotated = new Matrix[4];
	
	public PuzzlePiece() {}

	public void setPuzzleMatrix(int x, int y) {
		imageMatrix = new int[y][x];
	}
	
	public Gender getGender(int index) {
		return this.rotated[index].gender;
	}
	
	public void setBoundingBoxCoordinatesFromOriginalPicture(int minX, int maxX, int minY, int maxY) {
		this.minX = minX;
		this.maxX = maxX;
		this.minY = minY;
		this.maxY = maxY;
	}

	public void setNewAnglePoint(Pixel newAnglePoint) {
		this.newAnglePoint = newAnglePoint;
	}

	public Pixel getNewAnglePoint() {
		return this.newAnglePoint;
	}

	public void setNorth(int northIndex) {
		this.northIndex = northIndex;
	}

	public void setOriginPosition(int x, int y) {
		this.originPosition = new Pixel(x, y);
	}

	public void setNumber(int i) {
		this.puzzlePieceNumber = i;
	}
	
	public Pixel getOriginalCoordinates() {
		return this.originPosition;
	}
}
