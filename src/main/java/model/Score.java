package model;

/**
 * The smaller the score, the better the match.
 */
public class Score {

	public final int score;
	public final Matrix matrix;
	
	public Score(Matrix m, int score) {
		this.matrix = m;
		this.score = score;
	}
}
