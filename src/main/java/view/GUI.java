package view;

import java.io.File;
import java.io.IOException;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import controller.GUIController;

/**
 * GUI class for the algorithm. Contains main.
 */
public class GUI extends Application {
	
	public static void main(String[] args) {
		launch(args);
	}

	TextField pathTextField;
	Text statusText;
	
	StackPane imageViewPane;
	
	VBox puzzleSolutionBox;
	
	ImageView puzzleImageView;
	public final int IMAGEVIEW_WIDTH = 1000;
	public final int IMAGEVIEW_HEIGHT = 600;
	
	@Override
	public void start(Stage primaryStage) throws IOException {
		
		BorderPane borderPane = setupGUI();
				
		Scene scene = new Scene(borderPane, 1280, 800);
		scene.getStylesheets().add(GUI.class.getResource("stylesheet.css").toExternalForm());
		        
		primaryStage.setScene(scene);
		primaryStage.setTitle("Puzzle Solver");
		primaryStage.show();
	}
	
	private BorderPane setupGUI() {
		
		BorderPane borderPane = new BorderPane();
		
		Button browseButton = new Button("Browse");
		browseButton.setOnMouseReleased(new LoadImageHandler());
		
		Label loadImageLabel = new Label("Load an Image...");
		loadImageLabel.setTextFill(Color.YELLOW);
		pathTextField = new TextField();
		pathTextField.setPrefWidth(800);
		
		Button assembleButton = new Button("Assemble!");
		assembleButton.setOnMouseReleased(new AssembleHandler());

		Button helpButton = new Button("Help");
		helpButton.setOnMouseReleased(new HelpHandler());
		
		HBox buttonBox = new HBox(20);
		buttonBox.getChildren().addAll(helpButton, pathTextField, browseButton, assembleButton);
		buttonBox.setAlignment(Pos.CENTER);
		
		VBox topBox = new VBox();
		topBox.setAlignment(Pos.CENTER);
		topBox.getChildren().addAll(loadImageLabel, buttonBox);
		
		Label statusLabel = new Label("Status:");
		statusLabel.setTextFill(Color.YELLOW);
		
		puzzleImageView = new ImageView();
		
		statusText = new Text("Waiting for work...");
		statusText.setFill(Color.LIGHTGREEN);
		statusText.setFont(new Font("Verdana", 20));
		statusText.setStyle("-fx-font-weight: bold;");
		
		VBox rightBox = new VBox(20); //Currently empty...
		
		VBox leftBox = new VBox(10); //Currently empty...
		
		VBox bottomBox = new VBox(20);
		bottomBox.getChildren().addAll(statusLabel, statusText);
		bottomBox.setAlignment(Pos.CENTER);
		
		puzzleSolutionBox = new VBox();
		puzzleSolutionBox.setAlignment(Pos.CENTER);
		
		imageViewPane = new StackPane();
		imageViewPane.getChildren().addAll(puzzleImageView, puzzleSolutionBox);
		
		borderPane.setLeft(leftBox);
		borderPane.setCenter(imageViewPane);
		borderPane.setRight(rightBox);
		borderPane.setTop(topBox);
		borderPane.setBottom(bottomBox);
		
		return borderPane;
	}
	
	private class LoadImageHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent arg0) {
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Load a puzzle image...");
			String path = fileChooser.showOpenDialog(new Stage()).toString();
			pathTextField.setText(path);
			
			File file = new File(path);
	        Image image = new Image(file.toURI().toString(), IMAGEVIEW_WIDTH, IMAGEVIEW_HEIGHT, false, true);
//	        double height = image.getHeight();
//	        double width = image.getWidth();
//	        double ratio = width / height;
	        
			puzzleImageView.setImage(image);
		}
	}
	
	private class AssembleHandler implements EventHandler<MouseEvent> {
		
		@Override
		public void handle(MouseEvent arg0) {
			GUIController ctrl = new GUIController(GUI.this, pathTextField.getText());
			ctrl.assemble();
		}
	}
	
	private class HelpHandler implements EventHandler<MouseEvent> {
		
		@Override
		public void handle(MouseEvent arg0) {
			Stage helpWindow = new Stage();
			BorderPane borderPane = new BorderPane();
			
			TextArea ta = new TextArea(""
					+ "Browse = Choose a scan (image file), that contains puzzle pieces from a complete puzzle.\n"
					+ "Assemble = Match all the pieces together and find the solution. There has to be a given\n"
					+ "path that this can work."
					);
			ta.setEditable(false);
			borderPane.setCenter(ta);
			
			Scene windowScene = new Scene(borderPane, 700, 300);
			windowScene.getStylesheets().add(GUI.class.getResource("stylesheet.css").toExternalForm());
			helpWindow.setScene(windowScene);
			helpWindow.show();
		}
	}
	
	public void setStatus(String status) {
		statusText.setText(status);
	}
	
	public StackPane getImageViewPane() {
		return this.imageViewPane;
	}

	public void addSolutionEntry(Text text) {
		puzzleSolutionBox.getChildren().add(text);
	}
}
