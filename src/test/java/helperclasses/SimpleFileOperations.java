package helperclasses;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.List;
import model.PuzzlePiece;

/**
 * This class is meant to be a help for developers. It contains methods that write
 * matrices into txt-files on the disk. With those methods it's easier to debug, if
 * (for example) found corners look valid.
 * 
 * Recommended: Notepad++ and the maximum possible zoomed out view.
 */
public class SimpleFileOperations {
	
	private static final String WORKING_DIRECTORY = "C:/Projekt";
	
	/**
	 * Print a list of puzzle pieces into a txt file.
	 */
	public static void visualizeResult(List<PuzzlePiece> pplist) {
		StringBuilder r = new StringBuilder(1000000);
		for (int j=0; j<pplist.size(); ++j) {
			for (int i=0; i<4; ++i) {
				r.append(String.valueOf(j+1) + ":\n");
				for (int y=0; y<pplist.get(j).rotated[i].matrix.length; ++y) {
					for (int x=0; x<pplist.get(j).rotated[i].matrix[y].length; ++x) {
						r.append(pplist.get(j).rotated[i].matrix[y][x]);
					}
					r.append(System.getProperty("line.separator"));
				}
				r.append("\n\n\n\n\n");
			}
			r.append("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		}
				
		StringBuilder ur = new StringBuilder(1000000);
		for (int j=0; j<pplist.size(); ++j) {
			for (int y=0; y<pplist.get(j).imageMatrix.length; ++y) {
				for (int x=0; x<pplist.get(j).imageMatrix[y].length; ++x) {
					ur.append(pplist.get(j).imageMatrix[y][x]);
				}
				ur.append("\n");
			}
			ur.append("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		}
		
		toTxt(ur.toString(), "Unrotated_PuzzlePieces.txt");
		toTxt(r.toString(), "Rotated_PuzzlePieces.txt");
	}
	
	/**
	 * Copies a 2D-array to the working path, using the filename given as parameter.
	 * 
	 * @param arr
	 * @param fileName
	 */
	public static void ArrayToTxt(int[][] arr, String fileName) {
		StringBuilder sb = new StringBuilder();
		for (int y=0; y<arr.length; ++y) {
			for (int x=0; x<arr[0].length; ++x) {
				sb.append(arr[y][x]);
			}
			sb.append("\n");
		}
		
		toTxt(sb.toString(), fileName);
	}
	
	
	/**
	 * Writes a String into a txt file on the working directory.
	 */
	private static void toTxt(String t, String fileName) {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + fileName));
			bw.write(t + System.getProperty("line.separator"));
			bw.flush();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Writes 2 2D-arrays (usually the rotated sides) to the file system.
	 * 
	 * @param arr1 = First array (main, rotated180)
	 * @param arr2 = Second array (other, no rotated for matching)
	 * @param fileName
	 */
	public static void visualizeMatchingPieces(int[][] arr1, int[][] arr2, String fileName) {
		StringBuilder sb = new StringBuilder();
		for (int y=0; y<arr1.length; ++y) {
			for (int x=0; x<arr1[0].length; ++x) {
				sb.append(arr1[y][x]);
			}
			sb.append("\n");
		}
		
		sb.append("\n");
		
		for (int y=0; y<arr2.length; ++y) {
			for (int x=0; x<arr2[0].length; ++x) {
				sb.append(arr2[y][x]);
			}
			sb.append("\n");
		}
		
		toTxtWithoutDeletingOldEntries(sb.toString(),  fileName);
	}
	
	/**
	 * Writes a list of puzzle pieces (unrotated) to the file system.
	 * 
	 * @param pplist
	 * @param fileName
	 */
	public static void writeUntrotatedPieces(List<PuzzlePiece> pplist, String fileName) {
		
		StringBuilder s = new StringBuilder();
		for (PuzzlePiece pp : pplist) {
			for (int i = 0; i<pp.imageMatrix.length; ++i) {
				for (int j = 0; j<pp.imageMatrix[0].length; ++j) {
					s.append(pp.imageMatrix[i][j]);
				}
				s.append("\n");
			}
			s.append("\n\n\n\n");
		}
		
		toTxtWithoutDeletingOldEntries(s.toString(), fileName);
	}

	/**
	 * Writes a String into a file, without removing the already existing entries.
	 * 
	 * @param arrayString
	 * @param fileName
	 */
	private static void toTxtWithoutDeletingOldEntries(String arrayString, String fileName) {
		
		File file = new File(WORKING_DIRECTORY + fileName);
		
		BufferedWriter bw = null;
		try {
			
			StringBuilder sb = new StringBuilder();
			
			if (file.exists()) {
				BufferedReader br = new BufferedReader(new FileReader(file));
				String l;
				while ((l = br.readLine()) != null) {
					sb.append(l + "\n");
				}
				sb.append("\n\n\n");
				br.close();
			}
			
			bw = new BufferedWriter(new FileWriter(file));
			bw.write(sb.toString() + arrayString + "\n");
			bw.flush();
			bw.close();
		} catch (IOException e) {
			try { bw.close(); } catch (IOException e1) {}
			e.printStackTrace();
		}
	}

	/**
	 * Writes the border-sides or border pieces to a file.
	 * 
	 * @param bplist
	 * @param fileName
	 */
	public static void visualizePuzzlePiecesWithBorderIndex(List<PuzzlePiece> bplist, String fileName) {
		for (PuzzlePiece bp : bplist) {
			
			StringBuilder sb = new StringBuilder();
			for (int y=0; y<bp.rotated[Math.floorMod(bp.borderIndex, bp.rotated.length)].matrix.length; ++y) {
				for (int x=0; x<bp.rotated[Math.floorMod(bp.borderIndex, bp.rotated.length)].matrix[0].length; ++x) {
					sb.append(bp.rotated[Math.floorMod(bp.borderIndex, bp.rotated.length)].matrix[y][x]);
				}
				sb.append("\n");
			}
			
			sb.append("\n\n");
			
			
			toTxtWithoutDeletingOldEntries(sb.toString(), fileName);
		}
	}
	
	/**
	 * This method clears up old txt files.
	 * Reason: If the algorithm runs 3 times, there will be txt files that are gonna explode
	 * because some methods keep the old entries from txt files before writing new ones.
	 */
	public static void clearOldTXTFiles() {
		File f = new File(WORKING_DIRECTORY);
		File[] fa = f.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".txt") ? true : false;
			}
		});
		
		for (File temp : fa) {
			temp.delete();
		}
	}
}
