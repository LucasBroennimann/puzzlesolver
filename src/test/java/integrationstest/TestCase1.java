package integrationstest;


import static org.junit.Assert.assertTrue;

import org.junit.*;

import helperclasses.SimpleFileOperations;

import java.io.File;
import java.util.List;

import model.Element;
import model.PuzzlePiece;
import controller.initialisation.BinaryImage;
import controller.initialisation.PreparePuzzle;
import controller.preparation.FindCorners;
import controller.preparation.FindGender;
import controller.preparation.PuzzlePieceOperations;
import controller.preparation.RotationMatrix;
import controller.processing.matching.Matching;


/**
 * Integration-Test
 * 
 * Goes through the whole process and checks if the end-result is correct.
 */
public class TestCase1 {
	
	List<PuzzlePiece> pplist = null;

	@Ignore
	@Test
	public void testWholeAlgorithm() {
		
		String imagePath = "C:/Projekt/TestCase.png";
		
		SimpleFileOperations.clearOldTXTFiles();
		
		String binaryImagePath = BinaryImage.create(new File(imagePath).getParent(), new File(imagePath).getName());
		int [][] binImgAsArray = BinaryImage.to2DArray(binaryImagePath);
		List<PuzzlePiece> pplist = new PreparePuzzle().searchPuzzlePieces(binImgAsArray);
		new FindCorners().findTheCornersForEachPuzzlePiece(pplist);
		new RotationMatrix().rotateAllPieces(pplist);
		PuzzlePieceOperations ppo = new PuzzlePieceOperations();
		ppo.repairHoles(pplist);
		new FindGender().findGender(pplist);
		Element firstCorner = new Matching().matchPieces(pplist);
		
		
		Element cur = firstCorner;
		assertTrue(cur.ppItem.puzzlePieceNumber == 3);cur = cur.right;
		assertTrue(cur.ppItem.puzzlePieceNumber == 15);cur = cur.right;
		assertTrue(cur.ppItem.puzzlePieceNumber == 11);cur = cur.right;
		assertTrue(cur.ppItem.puzzlePieceNumber == 2);cur = cur.right;
		assertTrue(cur.ppItem.puzzlePieceNumber == 7);cur = cur.down;
		
		assertTrue(cur.ppItem.puzzlePieceNumber == 5);cur = cur.left;
		assertTrue(cur.ppItem.puzzlePieceNumber == 6);cur = cur.left;
		assertTrue(cur.ppItem.puzzlePieceNumber == 14);cur = cur.left;
		assertTrue(cur.ppItem.puzzlePieceNumber == 10);cur = cur.left;
		assertTrue(cur.ppItem.puzzlePieceNumber == 9);cur = cur.down;
		
		assertTrue(cur.ppItem.puzzlePieceNumber == 8);cur = cur.right;
		assertTrue(cur.ppItem.puzzlePieceNumber == 1);cur = cur.right;
		assertTrue(cur.ppItem.puzzlePieceNumber == 12);cur = cur.right;
		assertTrue(cur.ppItem.puzzlePieceNumber == 13);cur = cur.right;
		assertTrue(cur.ppItem.puzzlePieceNumber == 4);cur = cur.right;
	}
}