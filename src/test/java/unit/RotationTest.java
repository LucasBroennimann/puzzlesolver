package unit;

import static org.junit.Assert.*;
import model.Matrix;
import model.Pixel;

import org.junit.*;

import controller.processing.Rotate;

public class RotationTest {

	int[][] rotateArray = {
			{ 0, 1, 1, 0, 1, 0 },
			{ 1, 0, 0, 1, 1, 1 },
			{ 0, 0, 0, 1, 1, 1 },
			{ 1, 1, 1, 1, 1, 1 },
			{ 1, 1, 1, 1, 1, 1 }
	};
	
	Pixel origCorner1 = new Pixel(0,0);
	Pixel origCorner2 = new Pixel(3,4);
	
	Matrix rotateMatrix = new Matrix(null, rotateArray);
	
	@Test
	public void testRotation() {
		
		rotateMatrix.corner1 = origCorner1;
		rotateMatrix.corner2 = origCorner2;
		
		Rotate.rotate180(rotateMatrix);
		
		assertFalse(rotateMatrix == null);
		assertFalse(rotateArray[0][0] == rotateMatrix.matrix[0][0]);
		assertFalse(rotateMatrix.corner1 == origCorner1);
		assertFalse(rotateMatrix.corner2 == origCorner2);
		
		Rotate.rotate180(rotateMatrix);
		
		for (int y=0; y < rotateMatrix.matrix.length; ++y) {
			for (int x=0; x < rotateMatrix.matrix[0].length; ++x) {
				assertTrue(rotateMatrix.matrix[y][x] == rotateArray[y][x]);
			}
		}
		
		assertTrue(rotateArray[0][0] == rotateMatrix.matrix[0][0]);
		assertTrue(rotateMatrix.corner1.getX() == origCorner1.getX() && rotateMatrix.corner1.getY() == origCorner1.getY());
		assertTrue(rotateMatrix.corner2.getX() == origCorner2.getX() && rotateMatrix.corner2.getY() == origCorner2.getY());
	}
}
